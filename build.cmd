rem dir /s Microsoft.Cpp.Default.props
rem dir C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Microsoft\VC\*

rem npm config set msvs_version 2017

rem set VCTargetsPath=C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\IDE\VC\VCTargets
rem set VCTargetsPath=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Microsoft\VC\v160

rem "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"
rem "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\MSBuild.exe" /m /p:Configuration=ReleaseNativeOnly /p:Platform=x64 SQLite.NET.2017.MSBuild.sln


npm i windows-build-tools@4.0.0
cd setup
call build.bat ReleaseNativeOnly x64
call build.bat ReleaseNativeOnly Win32
